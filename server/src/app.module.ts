import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsModule } from './news/news.module';

@Module({
  imports: [
    NewsModule,
    MongooseModule.forRoot(process.env.DB_CONNECTION_STRING || 'mongodb://localhost:27017', {
      useFindAndModify: false, authSource: 'admin'
    }),
  ],
})
export class AppModule {}
