import { Body, Controller, Get, Put } from '@nestjs/common';
import { Response } from './interfaces/Response';
import { NewsService } from './news.service';
import { News } from './schemas/news.schema';

@Controller('news')
export class NewsController {
  constructor(private readonly newsService: NewsService) {}

  @Put()
  async update(@Body() _id: string): Promise<{ removed: boolean } & Response> {
    const removed = await this.newsService.update(_id);
    return {
      status: 200,
      message: 'News removed.',
      removed,
    };
  }

  @Get()
  async findAll(): Promise<{ news: News[] } & Response> {
    const news = await this.newsService.findAll();
    return {
      status: 200,
      message: 'News returned.',
      news,
    };
  }
}
