import { Model } from 'mongoose';
import { HttpService, Injectable, OnApplicationBootstrap } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { News, NewsDocument } from './schemas/news.schema';
import { AxiosResponse } from 'axios';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PropsResponse } from './interfaces/PropsReponse';

@Injectable()
export class NewsService implements OnApplicationBootstrap {
  constructor(
    @InjectModel(News.name) private newsModel: Model<NewsDocument>,
    private httpService: HttpService,
  ) {}

  onApplicationBootstrap(): void {
    this.fetchNews();
  }

  async update(_id: string): Promise<boolean> {
    const result = await this.newsModel.updateOne({ _id }, { removed: true });
    return !!result.nModified;
  }

  async findAll(): Promise<News[]> {
    return await this.newsModel.find({ removed: false }).sort({ date: 'desc' }).exec();
  }

  fetchNews(): void {
    this.httpService
      .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .subscribe(async (resp: AxiosResponse<PropsResponse>) => {
        const news = resp.data.hits.map((item) => {
          const newsData = {
            title: item.story_title || item.title,
            author: item.author,
            date: item.created_at,
            url: item.story_url || item.url,
            _id: item.objectID,
          };
          return this.newsModel.findOneAndUpdate( { _id: newsData._id }, newsData, { upsert: true, setDefaultsOnInsert: true });
        });
        await Promise.all(news);
      });
  }

  @Cron(CronExpression.EVERY_HOUR)
  refreshNews(): void {
    this.fetchNews();
  }
}
