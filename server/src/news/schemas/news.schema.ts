import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NewsDocument = News & Document;

@Schema()
export class News {
  @Prop()
  title: string;

  @Prop()
  author: string;

  @Prop()
  url: string;

  @Prop()
  _id: string;

  @Prop()
  date: Date;

  @Prop({ default: false })
  removed?: boolean;
}

export const NewsSchema = SchemaFactory.createForClass(News);
