export interface PropsNews {
  title?: string;
  url?: string;
  story_title?: string;
  story_url?: string;
  author: string;
  created_at: Date;
  objectID: string;
}
