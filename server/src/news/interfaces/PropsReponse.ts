import { PropsNews } from './PropsNews';

export interface PropsResponse {
  hits: PropsNews[];
}
