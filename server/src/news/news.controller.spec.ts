import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';

describe('NewsController', () => {
  let app: TestingModule;
  let newsService = {
    findAll: () => ['test'],
    update: (_id: string) => false || true,
  };
  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [NewsService],
    })
      .overrideProvider(NewsService)
      .useValue(newsService)
      .compile();
  });

  describe('findAll', () => {
    it('should return array of news', async () => {
      const newsController = app.get<NewsController>(NewsController);
      const result = ['test'];
      jest.spyOn(newsService, 'findAll').mockImplementation(() => result);
      const { news } = await newsController.findAll();
      expect(news).toBe(result);
    });
  });

  describe('update', () => {
    it('should return boolean', async () => {
      const newsController = app.get<NewsController>(NewsController);
      const { removed } = await newsController.update('25526708');
      expect(removed).toBeTruthy();
    });
  });
});
