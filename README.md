# Reign Test

To start the project run the command on the root folder of the project:

```docker-compose up --detach```

And open http://localhost:3001 in your browser when the docker containers are all created and running.

client is routed to port 3001, server to port 8080 and mongodb to port 27017.

Thank you for the opportunity! Waiting for your answer.
