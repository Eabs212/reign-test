export interface News {
	title?: string;
	url?: string;
	author: string;
	date: Date;
	_id: string;
}