import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { News } from "../interfaces/News";
import { getNews, removeNews } from "../services/news";
import ItemList from "./ItemList";
import Spinner from "../assets/spinner.svg";

const Container = styled.div`
	flex: 1;
	overflow-y: auto;
`;

const SpinnerContainer = styled.div`
	display: flex;
	justify-content: center;
`;

const List: React.FC = () => {
	const [news, setNews] = useState<News[]>([]);
	const [loading, setLoading] = useState<boolean>(false);

	useEffect(() => {
		fetchNews();
	}, []);

	const fetchNews = async () => {
		setLoading(true);
		try {
			const news = await getNews();
			setNews(news);
		} catch (e) {
			// Handle error message
			console.log(e);
		} finally {
			setLoading(false);
		}
	};

	const removeNewsHandler = async (id: string) => {
		try {
			const deleted = await removeNews(id);
			console.log(deleted);
			deleted
				? setNews((news) => news.filter((item) => item._id !== id))
				: console.log("Not Found"); // Handle news not found
		} catch (e) {
			// Handle error message
			console.log(e);
		}
	};

	return (
		<Container>
			{loading ? (
				<SpinnerContainer>
					<img src={Spinner} alt="spinner" />
				</SpinnerContainer>
			) : (
				news.map((item) => (
					<ItemList {...item} key={item._id} removeNews={removeNewsHandler} />
				))
			)}
		</Container>
	);
};

export default List;
