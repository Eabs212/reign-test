import moment from "moment";
import React from "react";
import styled from "styled-components";
import { News } from "../interfaces/News";
import TrashIcon from "./icons/TrashIcon";

const Container = styled.div`
  background-color: #fff;
  font-size: 13pt;
  display: flex;
  justify-content: space-between;
  border: 1px #ccc solid;
  border-right: none;
  border-left: none;
  margin: 0;
  margin-left: 50px;
  margin-right: 20px;
  padding: 20px 0px;

  &:hover {
    background-color: #fafafa;
    cursor: pointer;
  }
`;

const Title = styled.span`
  color: #333;
`;

const Author = styled.span`
  color: #999;
`;

const Time = styled(Title)`
  margin-right: 30px;
`;

const TimeContainer = styled.div`
  display: flex;
`;

const ItemList: React.FC<ItemListProps> = ({ title, author, date, url, removeNews, _id }) => {
  const handleClick = (e: React.MouseEvent<SVGSVGElement, MouseEvent>, id: string) => {
    e.stopPropagation();
    removeNews(id);
  }

  return (
    <Container onClick={() => window.open(url, '_blank')}>
      <div>
        <Title>{title}</Title>
        <Author> {` - ${author} - `} </Author>
      </div>
      <TimeContainer>
        <Time>{moment(date).calendar()}</Time>
        <TrashIcon onClick={(e) => handleClick(e, _id)}/>
      </TimeContainer>
    </Container>
  );
};


export default ItemList;

interface ItemListProps extends News {
  removeNews: (id: string) => Promise<void>
}
