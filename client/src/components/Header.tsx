import React from "react";
import styled from "styled-components";

const Container = styled.div`
	font-family: "Nunito", sans-serif;
	background-color: #343ea0;
	color: white;
	width: 100%;
	height: 250px;
	display: flex;
	flex-direction: column;
	justify-content: center;
`;

const Title = styled.h1`
	font-size: 66px;
	font-weight: bold;
	padding-left: 50px;
	margin: 0;
`;

const SubTitle = styled.span`
	font-size: 21px;
	padding-left: 50px;
`;

const Header: React.FC = () => {
	return (
		<Container>
			<Title>HN Feed</Title>
			<SubTitle>{`We <3 hacker news!`}</SubTitle>
		</Container>
	);
};

export default Header;
