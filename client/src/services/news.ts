import axios from "axios";
import { News } from "../interfaces/News";
import { Response } from "../interfaces/Response";

const server = process.env.REACT_APP_SERVER_URL;

export const getNews = async (): Promise<News[]> => {
	const response = await axios.get<{ news: News[] } & Response>(`${server}/news`);
	return response.data.news;
};

export const removeNews = async (id: string): Promise<boolean> => {
	const response = await axios.put<{ removed: boolean } & Response>(`${server}/news`, { _id: id });
	return response.data.removed;
};

